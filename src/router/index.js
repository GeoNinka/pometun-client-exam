import Vue from 'vue'
import VueRouter from 'vue-router'
import MainView from '../views/MainView.vue'
import FormView from '../views/FormView.vue'
import RecordView from '../views/RecordView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Main',
    component: MainView
  },
  {
    path: '/records',
    name: 'recording',
    component: RecordView
  },
  {
    path: '/form/:service',
    name: 'Form',
    component: FormView
  },
  {
    path: '/form',
    name: 'Form',
    component: FormView
  },
  {
    path: '/anchor',
    name: 'Form',
    redirect: '/'
  }
]

const router = new VueRouter({
  routes,
  scrollBehavior() {
    return {x:0, y: 0}
  }
})

export default router
